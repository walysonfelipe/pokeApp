export class Pokemon {
    pokeIndex: Number;
    name: String;
    image:String;
    favorite?: Boolean;
}
