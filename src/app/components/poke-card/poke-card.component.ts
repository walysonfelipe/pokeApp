import { Component, Input, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Pokemon } from 'src/app/class/pokemon';
import { FavoriteService } from '../../services/favorite/favorite.service';
@Component({
  selector: 'poke-card',
  templateUrl: './poke-card.component.html',
  styleUrls: ['./poke-card.component.scss'],
})
export class PokeCardComponent implements OnInit {
  @Input()
  pokeData: Pokemon;

  constructor(
    private favoriteService: FavoriteService,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  async addFavorite(poke:Pokemon){  
    this.favoriteService.add(poke.pokeIndex,poke.name, poke.image);
    this.pokeData.favorite = true;
    await this.toast('Pokemon favoritado com sucesso');
    }
 
   async removeFavorite(id:any){    
    this.favoriteService.removebyId(id);
    this.pokeData.favorite = false;
    await this.toast('Pokemon removido com sucesso');
   }

   async toast(message: string){
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      cssClass: 'custom-toast',
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    });
    await toast.present();
   }
}


