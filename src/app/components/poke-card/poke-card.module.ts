import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { PokeCardComponent } from '../poke-card/poke-card.component';
import { DirectivesModule } from '../../directives/directives.module';
@NgModule({
declarations: [
 PokeCardComponent,
],
imports: [IonicModule, CommonModule, DirectivesModule],
exports: [
  PokeCardComponent,
]
})
export class PokeCardModule { }