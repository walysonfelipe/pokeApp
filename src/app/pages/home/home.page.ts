import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonInfiniteScroll} from '@ionic/angular';
import { PokemonService } from '../../services/pokeApi/pokemon.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infinite: IonInfiniteScroll;

  offset = 0;
  pokemon:any[] = [];

  constructor(
    private pokeService: PokemonService, 
    private router: Router
    ) {
    }
  
  ngOnInit(): void {
    this.loadPokemon();
  }

  ionViewWillEnter(){
    this.pokemon = [];
    this.loadPokemon();
  }

  loadPokemon(loadMore:boolean =false,event?){
    if(loadMore){
      this.offset += 26;
    }

    this.pokeService.get(this.offset).subscribe(pokeData =>{
      this.pokemon = [...this.pokemon, ...pokeData];
                   
      if(event){
        event.target.complete();
      }
    });
  }

  details(id:any){
    this.router.navigate(["/home/" + id])
  }

  pokeFav(){
    this.router.navigate(["/favorite"])   
  }

  onSearchChange(e) {
    let value = e.detail.value;
    
    if (value == '') {
      this.offset = 0;
      this.loadPokemon();
      return;
    }

    this.pokeService.find(value).subscribe((pokemon)=>{
      this.pokemon = [pokemon];
    }, err => {
        this.pokemon = [];
      });
  
} 

}