import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoritePageRoutingModule } from './favorite-routing.module';

import { FavoritePage } from './favorite.page';
import { PokeCardModule } from '../../components/poke-card/poke-card.module';
import { DirectivesModule } from '../../directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavoritePageRoutingModule,
    DirectivesModule,
    PokeCardModule
  ],
  declarations: [FavoritePage]
})
export class FavoritePageModule {}
