import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FavoriteService} from '../../services/favorite/favorite.service'

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.page.html',
  styleUrls: ['./favorite.page.scss'],
})
export class FavoritePage implements OnInit {
  pokeFav:any = [];
  constructor(
    private favorite: FavoriteService,
    private router: Router
    ) {  
  }

  ionViewWillEnter(){
    this.getPokemonsFavorites(); 
  }

  ngOnInit() {
  }
  
  
  getPokemonsFavorites(){
       this.favorite.get().then((pokeData: any) =>{
              this.pokeFav = pokeData
       });
  }

  home(){
    this.router.navigate(["/home"])  
  }

  details(id:any){
    this.router.navigate(["/home/" + id])
  }
}
