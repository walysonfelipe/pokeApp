import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { PokemonService } from '../../services/pokeApi/pokemon.service';
import { FavoriteService } from '../../services/favorite/favorite.service'
import SwiperCore, { Lazy  } from 'swiper';


SwiperCore.use([ Lazy ]);
@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailsPage implements OnInit {
  details:any;
  favorite: boolean;
  index: any;
  constructor(
    private route:  ActivatedRoute, 
    private router:Router,
    private toastController: ToastController,
    private pokeService: PokemonService,
    private favoriteService: FavoriteService
    ) {
     this.index = this.route.snapshot.paramMap.get('index'); 
     }

  ngOnInit() {
  }

  ionViewWillEnter(){    
    this.details = [];
    this.getDetailsPoke(this.index);
    this.getFavorite(this.index);
  }

  getDetailsPoke(index){
    this.pokeService.details(index).subscribe((poke:any) =>{
      this.details.push(poke);
    })
  }

  home(){
    this.router.navigate(["/home"]);
  }

  getFavorite(index){
    this.favoriteService.getById(index).then((data:any)=>{
      this.favorite = data;
    });  
  }

  async addFavorite(index:any, detail:any){
    this.favoriteService.add(index, this.details[0].name, this.details[0].images[0].image_front);
    this.favorite = true;
     await this.toast('Pokemon favoritado com sucesso');
    }

  async removeFavorite(index:any){
    this.favoriteService.removebyId(index);
    this.favorite = false;
    await this.toast('Pokemon removido com sucesso');
  }

  async toast(message: string){
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      cssClass: 'custom-toast',
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    });
    await toast.present();
   }
}
