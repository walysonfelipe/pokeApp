import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'

import { FavoriteService} from '../favorite/favorite.service';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private baseUrl:String = 'https://pokeapi.co/api/v2';
  private imageUrl:String = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon'
  constructor(private http: HttpClient, private favoriteService: FavoriteService) { }
  
  get(offset = 0){
    return this.http.get(`${this.baseUrl}/pokemon?offset=${offset}&limit=26`).pipe(
      map(result => {
          return result['results'];
      }),
      map(pokemons =>{
        return pokemons.map((poke: { image: string; pokeIndex: any; favorite:boolean }, index: number) => {
          poke.image = this.getPokeImage(index + offset + 1);
          poke.pokeIndex = offset + index + 1;
          this.favoriteService.getById(poke.pokeIndex).then((op:any)=> { poke.favorite = op })
          return poke;
        });
      })
    )
  }

  private getPokeImage(index){
     return `${this.imageUrl}/${index}.png`;
  }
  
  details(index: number){
      return this.http.get(`${this.baseUrl}/pokemon/${index}`).pipe(
        map(poke => {  
          let images  = [];
          images.push(
          {
            image_front : poke['sprites'].front_default,
            image_back : poke['sprites'].back_default
          }
          );
            
          poke['images'] = images;
            
          return poke;
        }))
  }

 find(search:string){
  return this.http.get(`${this.baseUrl}/pokemon/${search}`).pipe(
    map(pokemon => {
      pokemon['pokeIndex'] = pokemon['id'];
      pokemon['image'] = this.getPokeImage(pokemon['id']);
      return pokemon;
    })
  ); 
 }

}
