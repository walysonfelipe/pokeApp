import { Injectable } from '@angular/core';
import { NativeStorage } from '@awesome-cordova-plugins/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {

  constructor(private nativeStorage: NativeStorage) {}

  async get(){
    return this.nativeStorage.getItem('favorite')
      .then((data:any) => {
           return data;
      }).catch(() =>{
         return [];
      })
      ; 
  }

   async getById(id:any, details?:any) {
       return this.get().then((favoriteStorage: any):any =>{
                  
          return favoriteStorage.some((poke:any, i:number) => {
            return poke.pokeIndex == id ? true :  !details ? false : true;
          });
        });
  }
  
  private async setData(data:any){
    this.nativeStorage.setItem('favorite', data)      
  }

  async add(id:any,name: String, image: String){
    return this.get().then((favoriteStorage: any) =>{

      if(!favoriteStorage){
        this.setData({
          pokeIndex: id,
          name: name,
          image: image,
          favorite: true         
        })
      }
        else{
          const data = [].concat(favoriteStorage, {
            pokeIndex: id,
            name: name,
            image: image,
            favorite: true    
          });
         this.setData(data);
        }
     }).catch(err =>{
      console.error(err);
     })

  }
  
  async removebyId(id:any){
    return this.get().then((favoriteStorage: any):any =>{
        let removeItem = favoriteStorage.findIndex((poke: { pokeIndex: Number }) => poke.pokeIndex == id);
        favoriteStorage.splice(removeItem, 1);
       this.setData(favoriteStorage);
    });
  }
}
