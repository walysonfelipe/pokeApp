# PokeApp
![pokeApp](.github/assets/poke.png)
![GitHub repo size](https://img.shields.io/github/repo-size/walysonfelipe/pokeApp?style=for-the-badge)
![GitHub language count](https://img.shields.io/github/languages/count/walysonfelipe/pokeApp?style=for-the-badge)

O projeto desenvolvido para a vaga de Dev mobile Ionic no qual consiste em uma app que consome o [PokeApi](https://pokeapi.co/), utiliza-se o framework Ionic 6 + Angular.


<img src=".github/assets/home.png" width="150" />

> Tela inicial listando pokemons com paginação infinita, com possiblidade de pesquisa e favoritar.

<img src=".github/assets/pokeFav.png" width="150" />

> Tela favoritos listando pokemons favoritos.

<img src=".github/assets/detalhes.png" width="150" />

> Tela de detalhes do pokemon escolhido.

Projeto no figma [clique aqui](https://www.figma.com/file/epBEYYU9eJ6mNRpRD6gWAz/pokeApp?node-id=0%3A1)


### 💻 Plugins utilizados

- swiper
- cordova-plugin-nativestorage / @awesome-cordova-plugins/native-storage
- cordova-plugin-statusbar / @awesome-cordova-plugins/status-bar

### 🚀 Instalação

Clone o projeto

```bash
 $ git clone https://github.com/walysonfelipe/pokeApp.git
```

Entre no diretório do projeto

```bash
  $ cd pokeApp
```

Instale as dependências

```bash
  $ npm install
```

Configuração do capacitor 

```bash
   $ ionic build

   $ ionic cap add android   #adicionar plataforma android

   $ ionic cap copy

   $ ionic cap sync

```

Fazer deploy do android

```bash
   $ ionic cap open android 
```

<img src=".github/assets/androidstudio.png" width="500" />

> Adicionar Permissions  - app/manifests/AndroidManifest.xml

```bash
  <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
  <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

Testar aplicativo

```bash
   $ ionic cap run android 
   $ ionic cap run android -l --external # Live Reload 
```

##### Criado por :
<table>
  <tr>
    <td align="center">
      <a href="#">
        <img src="https://avatars.githubusercontent.com/u/35854466?s=460&u=184d9d1d89140c723af0cdbdaa604ce0ff42a28a&v=4" width="100px;" alt="Foto do walyson Felipe no GitHub"/><br>
        <sub>
          <b>{Walyson}</b>
        </sub>
      </a>
    </td>
    <tr>
   </table>